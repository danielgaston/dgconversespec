#
# Be sure to run `pod lib lint DGConverse.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DGConverse'
  s.version          = '0.1.0'
  s.summary          = 'Operation-based networking framework.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'The Network Layer uses NSOperations as wrapper for each particular a network call (data, upload or download). Same for other tasks as caching and data conversion, which now are completely independent operations; facilitating their reuse of in other projects.'
  s.homepage         = 'https://gitlab.com/danielgaston/converse.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'danielgaston' => 'gashsh@yahoo.es' }
  s.source           = { :git => 'https://gitlab.com/danielgaston/converse.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'DGConverse/**/*.{h,m}'
  s.prefix_header_contents = '#import "DGConverse.h"'
  # s.resource_bundles = {
  #   'DGConverse' => ['DGConverse/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'JSONModel'
  s.dependency 'SPTPersistentCache'
  
end